import 'package:tiny_http/tiny_http.dart';

void main() async{
  var server = Server<AppChannel>('localhost', 8888);
  await server.start(timeout: Duration(seconds: 120));
}



class AppChannel extends ApplicationChannel{
  @override
  void prepare() async{

  }
  @override
  Router getEndpoint() {
    var router = Router();
    router.route('test_endpoint')
        .link(() => Endpoint());
    return router;
  }
  @override
  void finalise() {
  }
}

class Endpoint extends Controller{
  @override
  Future<RequestOrResponse> handle (Request request) async{
    return Response.ok();
  }
}